import unittest
import random
import time
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException

class FirstPage(unittest.TestCase):

    @classmethod
    def setUp(self):
        # create a new Chrome session
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(30)
        self.driver.maximize_window()
        # navigate to the application home page
        self.driver.get("http://127.0.0.1:5000/")

    def test_sum_funds_ok(self):
        """
        Should check that the sum is ok
        """
        # get the search textbox
        self.monthly_salary_field = self.driver.find_element_by_id("formatted-numberformat-input")

        random_salary = random.randint(1, 10000000)
        # enter search keyword and submit
        self.monthly_salary_field.send_keys(random_salary)
        
        pension_fund_checkbox = self.driver.find_element_by_xpath("//input[@value='pensionFund']")
        managers_insurance_checkbox = self.driver.find_element_by_xpath("//input[@value='managersInsurance']")
        provident_fund_checkbox = self.driver.find_element_by_xpath("//input[@value='providentFund']")
        
        num_checkboxes = random.randint(1, 3) # Get random number of checkboxes to check
        selected_checkboxes = random.sample([pension_fund_checkbox, managers_insurance_checkbox, provident_fund_checkbox], num_checkboxes)
        
        # Click the checkboxes
        for i in range(num_checkboxes):
            selected_checkboxes[i].click()
        
        self.driver.implicitly_wait(1)
        try:
            pension_fund_textbox = self.driver.find_element_by_id("pension-fund")
        except NoSuchElementException:
            pass
        try:
            managers_insurance_textbox = self.driver.find_element_by_id("managers-insurance")
        except NoSuchElementException:
            pass
        try:
            provident_fund_textbox = self.driver.find_element_by_id("provident-fund")
        except NoSuchElementException:
            pass
        
        # fill the checkboxes with random values
        current_max_value = random_salary
        for i in range(num_checkboxes-1):
            value = random.randint(1, current_max_value)
            
            if selected_checkboxes[i].get_attribute("value") == "pensionFund":
                selected_textbox = pension_fund_textbox
            elif selected_checkboxes[i].get_attribute("value") == "managersInsurance":
                selected_textbox = managers_insurance_textbox
            elif selected_checkboxes[i].get_attribute("value") == "providentFund":
                selected_textbox = provident_fund_textbox
                
            selected_textbox.send_keys(value)
            current_max_value = current_max_value - value
            
        if selected_checkboxes[-1].get_attribute("value") == "pensionFund":
            selected_textbox = pension_fund_textbox
        elif selected_checkboxes[-1].get_attribute("value") == "managersInsurance":
            selected_textbox = managers_insurance_textbox
        elif selected_checkboxes[-1].get_attribute("value") == "providentFund":
            selected_textbox = provident_fund_textbox
        selected_textbox.send_keys(current_max_value)
        
        
        done_button = self.driver.find_element_by_class_name("btn-sm")
        done_button.click()
        
        # Make sure landed successfully on the next page
        self.assertEqual(self.driver.find_element_by_tag_name("strong").text, "Funds Accumulation")
        
    def test_sum_funds_fail(self):
        """
        Should check that if the sum is not ok we do not continue
        """
        # get the search textbox
        self.monthly_salary_field = self.driver.find_element_by_id("formatted-numberformat-input")

        random_salary = random.randint(1, 10000000)
        # enter search keyword and submit
        self.monthly_salary_field.send_keys(random_salary)
        
        pension_fund_checkbox = self.driver.find_element_by_xpath("//input[@value='pensionFund']")
        managers_insurance_checkbox = self.driver.find_element_by_xpath("//input[@value='managersInsurance']")
        provident_fund_checkbox = self.driver.find_element_by_xpath("//input[@value='providentFund']")
        
        num_checkboxes = random.randint(1, 3) # Get random number of checkboxes to check
        selected_checkboxes = random.sample([pension_fund_checkbox, managers_insurance_checkbox, provident_fund_checkbox], num_checkboxes)
        
        # Click the checkboxes
        for i in range(num_checkboxes):
            selected_checkboxes[i].click()
        
        self.driver.implicitly_wait(1)
        try:
            pension_fund_textbox = self.driver.find_element_by_id("pension-fund")
        except NoSuchElementException:
            pass
            
        try:
            managers_insurance_textbox = self.driver.find_element_by_id("managers-insurance")
        except NoSuchElementException:
            pass
            
        try:
            provident_fund_textbox = self.driver.find_element_by_id("provident-fund")
        except NoSuchElementException:
            pass
        
        # fill the checkboxes with random values
        current_max_value = random_salary-1 # max values lacks 1 for incorrect price
        for i in range(num_checkboxes-1):
            value = random.randint(1, current_max_value)
            
            if selected_checkboxes[i].get_attribute("value") == "pensionFund":
                selected_textbox = pension_fund_textbox
            elif selected_checkboxes[i].get_attribute("value") == "managersInsurance":
                selected_textbox = managers_insurance_textbox
            elif selected_checkboxes[i].get_attribute("value") == "providentFund":
                selected_textbox = provident_fund_textbox
                
            selected_textbox.send_keys(value)
            current_max_value = current_max_value - value
            
        if selected_checkboxes[-1].get_attribute("value") == "pensionFund":
            selected_textbox = pension_fund_textbox
        elif selected_checkboxes[-1].get_attribute("value") == "managersInsurance":
            selected_textbox = managers_insurance_textbox
        elif selected_checkboxes[-1].get_attribute("value") == "providentFund":
            selected_textbox = provident_fund_textbox
        selected_textbox.send_keys(current_max_value)
        
        done_button = self.driver.find_element_by_class_name("btn-sm")
        time.sleep(5)
        done_button.click()
        
        # Make sure we get an error page
        self.assertEqual(self.driver.find_element_by_class_name("card-text").text, "Salary split not equal to monthly salary")

    @classmethod
    def tearDown(self):
        # close the browser window
        self.driver.quit()
        
class SecondPage(unittest.TestCase):

    @classmethod
    def setUp(self):
        # create a new Chrome session
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(30)
        self.driver.maximize_window()
        # navigate to the application home page
        self.driver.get("http://127.0.0.1:5000/")

    def test_correct_textboxes_appear(self):
        """
        Should check that the sum is ok
        """
        self.monthly_salary_field = self.driver.find_element_by_id("formatted-numberformat-input")

        salary = 3
        # enter search keyword and submit
        self.monthly_salary_field.send_keys(salary)
        
        pension_fund_checkbox = self.driver.find_element_by_xpath("//input[@value='pensionFund']")
        managers_insurance_checkbox = self.driver.find_element_by_xpath("//input[@value='managersInsurance']")
        provident_fund_checkbox = self.driver.find_element_by_xpath("//input[@value='providentFund']")
        
        num_checkboxes = random.randint(1, 3) # Get random number of checkboxes to check
        selected_checkboxes = random.sample([pension_fund_checkbox, managers_insurance_checkbox, provident_fund_checkbox], num_checkboxes)
        
        # Click the checkboxes
        for i in range(num_checkboxes):
            selected_checkboxes[i].click()
        
        self.driver.implicitly_wait(1)
        try:
            pension_fund_textbox = self.driver.find_element_by_id("pension-fund")
        except NoSuchElementException:
            pass
        try:
            managers_insurance_textbox = self.driver.find_element_by_id("managers-insurance")
        except NoSuchElementException:
            pass
        try:
            provident_fund_textbox = self.driver.find_element_by_id("provident-fund")
        except NoSuchElementException:
            pass
        
        # fill the checkboxes with random values
        pension_fund_selected = False
        provident_fund_selected = False
        managers_insurance_selected = False
        
        current_max_value = salary
        for i in range(num_checkboxes-1):
            value = random.randint(1, current_max_value)
            
            if selected_checkboxes[i].get_attribute("value") == "pensionFund":
                selected_textbox = pension_fund_textbox
                pension_fund_selected = True
            elif selected_checkboxes[i].get_attribute("value") == "managersInsurance":
                selected_textbox = managers_insurance_textbox
                managers_insurance_selected = True
            elif selected_checkboxes[i].get_attribute("value") == "providentFund":
                selected_textbox = provident_fund_textbox
                provident_fund_selected = True
                
            selected_textbox.send_keys(value)
            current_max_value = current_max_value - value
            
        if selected_checkboxes[-1].get_attribute("value") == "pensionFund":
            selected_textbox = pension_fund_textbox
            pension_fund_selected = True
        elif selected_checkboxes[-1].get_attribute("value") == "managersInsurance":
            selected_textbox = managers_insurance_textbox
            managers_insurance_selected = True
        elif selected_checkboxes[-1].get_attribute("value") == "providentFund":
            selected_textbox = provident_fund_textbox
            provident_fund_selected = True
        selected_textbox.send_keys(current_max_value)
        
        done_button = self.driver.find_element_by_class_name("btn-sm")
        done_button.click()
        import pdb; pdb.set_trace()
        if pension_fund_selected:
            self.driver.find_element_by_id("pensionFund")
        if provident_fund_selected:
            self.driver.find_element_by_id("managersinsurance")
        if managers_insurance_selected:
            self.driver.find_element_by_id("providentfund")
            
    @classmethod
    def tearDown(self):
        # close the browser window
        self.driver.quit()
        
if __name__ == '__main__':
    #unittest.main()
    
    first_page = unittest.TestLoader().loadTestsFromTestCase(FirstPage)
    second_page = unittest.TestLoader().loadTestsFromTestCase(SecondPage)
    test_suite = unittest.TestSuite([first_page, second_page])
    unittest.TextTestRunner(verbosity=2).run(test_suite)
