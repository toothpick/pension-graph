import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '36%',
        margin: 'auto',
        marginTop: 20,
    },
    button: {
        marginRight: theme.spacing(1),
    },
    instructions: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
}));

function getSteps() {
    return ['', '', ''];
}

export default function HorizontalLinearStepper(props) {
    const classes = useStyles();
    const [activeStep, setActiveStep] = React.useState(0);
    const [skipped, setSkipped] = React.useState(new Set());
    const steps = getSteps();

    return (
        <div className={classes.root}>
            <Stepper activeStep={props.activeStep} alternativeLabel style={{ backgroundColor: 'transparent' }}>
                {steps.map((label, index) => {
                    return (
                        <Step>
                            <StepLabel></StepLabel>
                        </Step>
                    );
                })}
            </Stepper>
        </div>
    );
}