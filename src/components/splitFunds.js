import React, { Component, Fragment } from "react";
import {
    MDBContainer, MDBRow, MDBCol, MDBJumbotron, MDBBtn, MDBIcon, MDBCardTitle, MDBCard, MDBCardText
} from 'mdbreact';

// Custom Number Field
import NumberFormatCustom from './customNumber';

// Material UI Imports
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';

// React Redux Imports
import { connect } from 'react-redux';

//Stepper
import Stepper from './stepper';

function mapStateToProps(state) {
    return {
        birthday: state.birthday,
        retireAge: state.retireAge,
        monthlySalary: state.monthlySalary,
        selectedValues: state.selectedValues
    };
}

class SplitFunds extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pensionFund: null,
            pensionFundAccumulation: true,
            managersInsurance: null,
            managersInsuranceAccumulation: true,
            providentFund: null,
            providentFundAccumulation: true,
            err: null,
        }
    }

    handlePensionChange = (event) => {
        let value = !this.state.pensionFundAccumulation;
        if (!value) {
            this.setState({
                pensionFundAccumulation: value,
                pensionFund: null
            })
        }
        else {
            this.setState({ pensionFundAccumulation: value })
        }
    }

    handleManagersChange = (event) => {
        let value = !this.state.managersInsuranceAccumulation;
        if (!value) {
            this.setState({
                managersInsuranceAccumulation: value,
                managersInsurance: null
            })
        }
        else {
            this.setState({ managersInsuranceAccumulation: value })
        }
    }

    handleProvidentChange = (event) => {
        let value = !this.state.providentFundAccumulation;
        if (!value) {
            this.setState({
                providentFundAccumulation: value,
                providentFund: null
            })
        }
        else {
            this.setState({ providentFundAccumulation: value })
        }
    }

    validate = () => {
        if (this.props.selectedValues.pensionFund && this.state.pensionFundAccumulation && this.state.pensionFund === null) {
            this.setState({ err: "Kindly enter Current Balance for Pension Fund" })
            return false;
        }
        else if (this.props.selectedValues.managersInsurance && this.state.managersInsuranceAccumulation && this.state.managersInsurance === null) {
            this.setState({ err: "Kindly enter Current Balance for Managers Insurance" })
            return false;
        }
        else if (this.props.selectedValues.providentFund && this.state.providentFundAccumulation && this.state.providentFund === null) {
            this.setState({ err: "Kindly enter Current Balance for Provident Fund" })
            return false;
        }
        return true;
    }

    clearErrors = () => {
        this.setState({ err: null })
    }

    onSubmit = () => {
        if (this.validate()) {
            this.props.dispatch({
                type: 'MAKE_CHARTS',
                pensionFund: this.state.pensionFund,
                managersInsurance: this.state.managersInsurance,
                providentFund: this.state.providentFund,
            })
            this.props.history.push("/linechart");
        }
    }

    render() {
        const { pensionFund, providentFund, managersInsurance } = this.state;
        return (
            <Fragment>
                <Stepper activeStep={1} />
                <MDBContainer className="w-50 tabs">
                    <MDBJumbotron>
                        <MDBCardTitle className="card-title text-center h4 pb-2">
                            <strong>Funds Accumulation</strong>
                        </MDBCardTitle>
                        {this.state.err ?
                            <Fragment>
                                <MDBCard color="red lighten-1" text="white" className="p-1">
                                    <MDBCardText>
                                        {this.state.err}
                                    </MDBCardText>
                                </MDBCard>
                                <br />
                            </Fragment>
                            :
                            null
                        }
                        {this.props.selectedValues.pensionFund ?
                            <MDBRow>
                                <MDBCol className="mt-5" md="12">
                                    Does Accumulation Exists in Pension Fund?
                                <RadioGroup
                                        id="pensionfund"
                                        aria-label="Does accumulation exists"
                                        name="Accumulation"
                                        value={this.state.pensionFundAccumulation}
                                        onChange={this.handlePensionChange}
                                        row
                                    >
                                        <FormControlLabel value={true} control={<Radio />} labelPlacement="end" label="Yes" />
                                        <FormControlLabel value={false} control={<Radio />} labelPlacement="end" label="No" />
                                    </RadioGroup>
                                </MDBCol>
                                {this.state.pensionFundAccumulation ?
                                    <MDBCol>
                                        <TextField
                                            required
                                            fullWidth
                                            value={pensionFund}
                                            label="Current Balance"
                                            id="formatted-numberformat-input"
                                            InputProps={{
                                                inputComponent: NumberFormatCustom,
                                            }}
                                            onChange={(e) => this.setState({ pensionFund: e.target.value })}
                                        />
                                    </MDBCol>
                                    : null}
                            </MDBRow>
                            : null}
                        {this.props.selectedValues.managersInsurance ?
                            <MDBRow>
                                <MDBCol className="mt-5" md="12">
                                    Does Accumulation Exists in Managers Insurance Fund?
                             <RadioGroup
                                        id="managersinsurance"
                                        aria-label="Does accumulation exists"
                                        name="Accumulation"
                                        value={this.state.managersInsuranceAccumulation}
                                        onChange={this.handleManagersChange}
                                        row
                                    >
                                        <FormControlLabel value={true} control={<Radio />} labelPlacement="end" label="Yes" />
                                        <FormControlLabel value={false} control={<Radio />} labelPlacement="end" label="No" />
                                    </RadioGroup>
                                </MDBCol>
                                {this.state.managersInsuranceAccumulation ?
                                    <MDBCol>
                                        <TextField
                                            required
                                            fullWidth
                                            value={managersInsurance}
                                            label="Current Balance"
                                            id="formatted-numberformat-input"
                                            InputProps={{
                                                inputComponent: NumberFormatCustom,
                                            }}
                                            onChange={(e) => this.setState({ managersInsurance: e.target.value })}
                                        />
                                    </MDBCol>
                                    : null}
                            </MDBRow>
                            : null}
                        {this.props.selectedValues.providentFund ?
                            <MDBRow>
                                <MDBCol className="mt-5" md="12">
                                    Does Accumulation Exists in Provident Fund?
                             <RadioGroup
                                        id="providentfund"
                                        aria-label="Does accumulation exists"
                                        name="Accumulation"
                                        value={this.state.providentFundAccumulation}
                                        onChange={this.handleProvidentChange}
                                        row
                                    >
                                        <FormControlLabel value={true} control={<Radio />} labelPlacement="end" label="Yes" />
                                        <FormControlLabel value={false} control={<Radio />} labelPlacement="end" label="No" />
                                    </RadioGroup>
                                </MDBCol>
                                {this.state.providentFundAccumulation ?
                                    <MDBCol>
                                        <TextField
                                            required
                                            fullWidth
                                            value={providentFund}
                                            label="Current Balance"
                                            id="formatted-numberformat-input"
                                            InputProps={{
                                                inputComponent: NumberFormatCustom,
                                            }}
                                            onChange={(e) => this.setState({ providentFund: e.target.value })}
                                        />
                                    </MDBCol>
                                    : null}
                            </MDBRow>
                            : null}
                        <br />
                        <br />
                        <div className="text-center">
                            <MDBBtn onClick={this.onSubmit} type="button" size="sm" outline color="teal accent-4">
                                Done <MDBIcon far icon="paper-plane" className="ml-1" />
                            </MDBBtn>
                        </div>
                    </MDBJumbotron>
                </MDBContainer >
            </Fragment>
        );
    }
}

export default connect(mapStateToProps)(SplitFunds);