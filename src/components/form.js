import React, { Component, Fragment } from "react";
import PropTypes from 'prop-types';
import {
    MDBContainer, MDBRow, MDBCol, MDBBtn, MDBIcon, MDBJumbotron, MDBCardTitle, MDBCard, MDBCardText
} from 'mdbreact';

// Import css
import '../styles/form.css';

// Material UI Imports
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import Checkbox from '@material-ui/core/Checkbox';

import { withStyles } from '@material-ui/core/styles';

// Date Picker
import DatePicker from 'react-date-picker';

// Custom Number Field
import NumberFormatCustom from './customNumber';

// React Redux Imports
import { connect } from 'react-redux';

//Stepper
import Stepper from './stepper';

function mapStateToProps(state) {
    return {
        birthday: state.birthday,
        retireAge: state.retireAge,
        monthlySalary: state.monthlySalary,
        pensionFundValue: state.pensionFundValue,
        managersInsuranceValue: state.managersInsuranceValue,
        providentFundValue: state.providentFundValue,
    };
}

const CustomCheckBox = withStyles({
    root: {
        color: '#00bfa5',
        '&$checked': {
            color: '#00bfa5',
        },
    },
    checked: {},
})(props => <Checkbox color="default" {...props} />);

NumberFormatCustom.propTypes = {
    inputRef: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
};

class FormPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            birthday: this.props.birthday,
            retireAge: this.props.retireAge,
            monthlySalary: this.props.monthlySalary,
            selectedValues: {
                pensionFund: false,
                managersInsurance: false,
                providentFund: false,
            },
            pensionFundValue: this.props.pensionFundValue || 0,
            managersInsuranceValue: this.props.managersInsuranceValue || 0,
            providentFundValue: this.props.providentFundValue || 0,
            splitSalary: false,
            err: null
        }
    }

    clearErrors = () => {
        this.setState({ err: null })
    }

    validate = () => {
        var pensionFund = this.state.selectedValues.pensionFund;
        var managersInsurance = this.state.selectedValues.managersInsurance;
        var providentFund = this.state.selectedValues.providentFund;

        if (this.state.birthday === null) {
            this.setState({ err: "Enter your birthday" })
            return false
        }
        else if (this.state.retireAge < 60 || this.state.retireAge > 80) {
            this.setState({ err: "Retire Age must be between 60 and 80" })
            return false
        }
        else if (this.state.monthlySalary === null || this.state.monthlySalary === "") {
            this.setState({ err: "Enter Monthly Salary" })
            return false
        }
        else if (this.state.monthlySalary <= 0) {
            this.setState({ err: "Monthly Salary must be greate than 1" })
            return false
        }
        else if ([pensionFund, managersInsurance, providentFund].filter(v => v).length === 0) {
            this.setState({ err: "You must select atleast one fund" })
            return false;
        }
        else if (
            (
                parseInt(this.state.pensionFundValue) + parseInt(this.state.managersInsuranceValue) + parseInt(this.state.providentFundValue)
            ) != this.state.monthlySalary) {
            this.setState({ err: "Salary split not equal to monthly salary" });
            return false;
        }
        return true
    }

    onDone = () => {
        if (this.validate()) {
            this.props.dispatch({
                type: 'SAVE_INFORMATION',
                selectedValues: this.state.selectedValues,
                monthlySalary: this.state.monthlySalary,
                birthday: this.state.birthday,
                retireAge: this.state.retireAge,
                pensionFundValue: this.state.pensionFundValue,
                managersInsuranceValue: this.state.managersInsuranceValue,
                providentFundValue: this.state.providentFundValue,
            })
            this.props.history.push("/splitfunds");
        }
    }

    onBirthdayChange = (event) => {
        var nowDate = event
        this.setState({ birthday: nowDate })
    }

    onRetireAgeChange = (event) => {
        var retireAge = event.target.value;
        this.setState({ retireAge: retireAge })
    }

    onMonthlySalaryChange = (event) => {
        var monthlySalary = event.target.value;
        this.setState({ monthlySalary: monthlySalary })
    }

    handleChange = name => event => {
        var checked = event.target.checked;
        let val = `${name}Value`;
        this.setState(prevState => ({
            selectedValues: {
                ...prevState.selectedValues,
                [name]: checked,
            },
            [val]: 0,
        }), () => console.log(this.state))
    };

    handleTextChange = e => {
        this.setState({
            [e.target.name]: e.target.value,
        })
    }

    render() {
        const { pensionFund, managersInsurance, providentFund } = this.state.selectedValues;
        return (
            <Fragment>
                <Stepper activeStep={0} />
                <MDBContainer className="w-75 form">
                    <MDBRow>
                        <MDBCol md="6">
                            <MDBJumbotron>
                                <MDBCardTitle className="card-title text-center h4 pb-2">
                                    <strong>Basic Information</strong>
                                </MDBCardTitle>
                                <form>
                                    {this.state.err ?
                                        <Fragment>
                                            <MDBCard color="red lighten-1" text="white" className="p-1">
                                                <MDBCardText>
                                                    {this.state.err}
                                                </MDBCardText>
                                            </MDBCard>
                                            <br />
                                        </Fragment>
                                        :
                                        null
                                    }
                                    <div className="grey-text">
                                        <MDBRow>
                                            <MDBCol className="mt-1" sm="6">
                                                <label style={{ fontSize: '13px' }} htmlFor="formGroupExampleInput">Birthday</label>
                                                <DatePicker
                                                    onChange={this.onBirthdayChange}
                                                    value={this.state.birthday}
                                                />
                                            </MDBCol>
                                            <MDBCol className="mt-3" md="6">
                                                <TextField
                                                    fullWidth
                                                    required
                                                    id="retire-age"
                                                    label="Retire Age"
                                                    type="number"
                                                    value={this.state.retireAge}
                                                    InputLabelProps={{
                                                        shrink: true
                                                    }}
                                                    InputProps={{ inputProps: { min: 60, max: 80 } }}
                                                    onChange={this.onRetireAgeChange}
                                                />
                                            </MDBCol>
                                        </MDBRow>
                                        <br />
                                        <MDBRow>
                                            <MDBCol md="12">
                                                <TextField
                                                    required
                                                    fullWidth
                                                    value={this.state.monthlySalary}
                                                    label="Monthly Salary"
                                                    id="formatted-numberformat-input"
                                                    InputProps={{
                                                        inputComponent: NumberFormatCustom,
                                                    }}
                                                    onChange={this.onMonthlySalaryChange}
                                                />
                                            </MDBCol>
                                        </MDBRow>
                                        <br />
                                        {this.state.monthlySalary ?
                                            <MDBRow>
                                                <MDBCol>
                                                    <FormControl required component="fieldset" >
                                                        <FormLabel component="legend">Split Monthly Salary</FormLabel>
                                                        <FormGroup className="ml-5">
                                                            <FormControlLabel
                                                                control={
                                                                    <CustomCheckBox onChange={this.handleChange('pensionFund')}
                                                                        checked={pensionFund} value="pensionFund" />
                                                                }
                                                                label="Pension Fund"
                                                            />
                                                            {pensionFund && <TextField
                                                                fullWidth
                                                                required
                                                                id="pension-fund"
                                                                label="Pension Fund"
                                                                type="number"
                                                                value={this.state.pensionFundValue}
                                                                name="pensionFundValue"
                                                                InputLabelProps={{
                                                                    shrink: true
                                                                }}
                                                                InputProps={{ inputProps: { min: 60, max: 80 } }}
                                                                onChange={this.handleTextChange}
                                                            />}
                                                            <FormControlLabel
                                                                control={
                                                                    <CustomCheckBox onChange={this.handleChange('managersInsurance')}
                                                                        checked={managersInsurance} value="managersInsurance" />
                                                                }
                                                                label="Managers Insurance"
                                                            />
                                                            {managersInsurance && <TextField
                                                                fullWidth
                                                                required
                                                                id="managers-insurance"
                                                                label="Managers Insurance"
                                                                type="number"
                                                                value={this.state.managersInsuranceValue}
                                                                name="managersInsuranceValue"
                                                                InputLabelProps={{
                                                                    shrink: true
                                                                }}
                                                                InputProps={{ inputProps: { min: 60, max: 80 } }}
                                                                onChange={this.handleTextChange}
                                                            />}
                                                            <FormControlLabel
                                                                control={
                                                                    <CustomCheckBox onChange={this.handleChange('providentFund')}
                                                                        checked={providentFund} value="providentFund" />
                                                                }
                                                                label="Provident Fund"
                                                            />
                                                            {providentFund && <TextField
                                                                fullWidth
                                                                required
                                                                id="provident-fund"
                                                                label="Provident Fund"
                                                                type="number"
                                                                value={this.state.providentFundValue}
                                                                name="providentFundValue"
                                                                InputLabelProps={{
                                                                    shrink: true
                                                                }}
                                                                InputProps={{ inputProps: { min: 60, max: 80 } }}
                                                                onChange={this.handleTextChange}
                                                            />}
                                                        </FormGroup>
                                                    </FormControl>
                                                </MDBCol>
                                            </MDBRow>
                                            :
                                            null
                                        }
                                    </div>

                                    <div className="text-center">
                                        <MDBBtn onClick={this.onDone} type="button" size="sm" outline color="teal accent-4">
                                            Done <MDBIcon far icon="paper-plane" className="ml-1" />
                                        </MDBBtn>
                                    </div>
                                </form>
                            </MDBJumbotron>
                        </MDBCol>
                    </MDBRow>
                </MDBContainer >
            </Fragment>
        );
    }
};

export default connect(mapStateToProps)(FormPage);