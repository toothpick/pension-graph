import React from "react";
import { Line, } from "react-chartjs-2";
import { MDBContainer } from "mdbreact";
import { format } from "date-fns";

function CalculateLabels(start, end) {
    let labels = [];
    let yearStart = start

    while (yearStart <= end) {
        labels.push(yearStart++);
    }
    return labels;
}

const commafy = (num) => {
    var str = num.toString().split('.');
    if (str[0].length >= 4) {
        str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1,');
    }
    if (str[1] && str[1].length >= 4) {
        str[1] = str[1].replace(/(\d{3})/g, '$1 ');
    }
    return str.join('.');
}



class ChartsPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            monthlyDeposit: this.props.monthlySalary,
            currentBalance: this.props.currentBalance,
            dataLine: {
                labels: ["January", "February", "March", "April", "May", "June", "July"],
                datasets: [this.datasets([])],
            }
        }
    }

    datasets = (data) => {
        return {
            label: this.props.name,
            fill: true,
            lineTension: 0.3,
            backgroundColor: this.props.backgroundColor,
            borderColor: this.props.color,
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: this.props.color,
            pointBackgroundColor: "#ffcdd2",
            pointBorderWidth: 5,
            pointHoverRadius: 3,
            pointHoverBackgroundColor: this.props.color,
            pointHoverBorderColor: "#bdbdbd",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: data
        }
    }

    GraphData = () => {
        let data = []
        let formula = 0.0;
        if (this.props.currentBalance) {
            // Current Accumulation
            formula = parseFloat(this.props.currentBalance)
            data.push(formula);
        } else {
            data.push(0);
        }
        let num = this.props.retireYear - this.props.startYear;
        for (let i = 1; i <= num; i++) {
            // Deposits minus monthly fee
            formula = formula + (parseFloat(this.state.monthlyDeposit) * 12) * (1 - (parseFloat(this.props.monthlyDepositFee) / 100));
            // Multiply by the net yield
            formula = formula * (1 + (parseFloat(this.props.yearlyYield) - parseFloat(this.props.accumulationFee)) / 100);
            // Push End Result to Graph
            data.push(formula);
        }
        return data;
    }

    componentDidMount() {
        let labels = CalculateLabels(this.props.startYear, this.props.retireYear);
        let data = this.GraphData();
        this.setState(prevState => ({
            dataLine: {
                ...prevState.dataLine,
                labels: labels,
                datasets: [this.datasets(data)],
            }
        }))
    }

    updateData = () => {
        let labels = CalculateLabels(this.props.startYear, this.props.retireYear);
        let data = this.GraphData();
        this.setState(prevState => ({
            dataLine: {
                ...prevState.dataLine,
                labels: labels,
                datasets: [this.datasets(data)],
            }
        }))
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.currentBalance !== this.props.currentBalance) {
            this.updateData();
        }
        if (prevProps.yearlyYield !== this.props.yearlyYield ||
            prevProps.accumulationFee !== this.props.accumulationFee ||
            prevProps.monthlyDepositFee !== this.props.monthlyDepositFee) {
            this.updateData();
        }
        if (prevProps.currentBalance !== this.props.currentBalance) {
            this.updateData();
        }
        if (prevProps.monthlySalary !== this.props.monthlySalary) {
            console.log("UPDATE VALUES: ", this.props.monthlySalary);
            this.updateData();
            this.setState({
                monthlyDeposit: this.props.monthlySalary
            })
        }
        if (prevState.monthlyDeposit !== this.props.monthlySalary && prevProps.name === "Complement Pension Fund") {
            console.log("UPDATE VALUES: ", this.props.monthlySalary);
            this.updateData();
            this.setState({
                monthlyDeposit: this.props.monthlySalary
            })
        }
    }

    render() {

        return (
            <MDBContainer>
                <Line data={this.state.dataLine} options={{
                    responsive: true,
                    tooltips: {
                        callbacks: {
                            label: function (tooltipItem, data) {
                                var label = data.datasets[tooltipItem.datasetIndex].label || '';

                                if (label) {
                                    label += ': ';
                                }

                                label += commafy(parseFloat(tooltipItem.value).toFixed(2))
                                return label
                            },

                        },

                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                // Include a dollar sign in the ticks
                                callback: function (value, index, values) {
                                    return commafy(value);
                                }
                            }
                        }],
                        xAxes: [{
                            ticks: {
                                // Include a dollar sign in the ticks
                                callback: function (value, index, values) {
                                    return commafy(value);
                                }
                            }
                        }]
                    }


                }} />
            </MDBContainer>
        );
    }
}

export default ChartsPage;