import React, { Fragment } from "react";
import ChartsPage from './chart';

import {
    MDBContainer, MDBRow, MDBCol, MDBJumbotron
} from 'mdbreact';

// Custom Number Field
import NumberFormatCustom from './customNumber';

// Material UI Imports
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import { Paper, Typography, Checkbox } from "@material-ui/core";

import { withStyles } from '@material-ui/core/styles';

// React Redux Imports
import { connect } from 'react-redux';

//Stepper
import Stepper from './stepper';

function mapStateToProps(state) {
    return {
        birthday: state.birthday,
        retireAge: state.retireAge,
        monthlySalary: state.monthlySalary,
        selectedValues: state.selectedValues,
        pensionFund: state.pensionFund,
        managersInsurance: state.managersInsurance,
        providentFund: state.providentFund,
        pensionFundValue: state.pensionFundValue,
        managersInsuranceValue: state.managersInsuranceValue,
        providentFundValue: state.providentFundValue,
    };
}

const CustomCheckBox = withStyles({
    root: {
        color: '#00bfa5',
        '&$checked': {
            color: '#00bfa5',
        },
    },
    checked: {},
})(props => <Checkbox color="default" {...props} />);


const commafy = (num) => {
    console.log(num)
    console.log(typeof (num))
    if (num) {
        var str = num.toString().split('.');
        if (str[0].length >= 4) {
            str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1,');
        }
        if (str[1] && str[1].length >= 4) {
            str[1] = str[1].replace(/(\d{3})/g, '$1 ');
        }
        return str.join('.');
    }
}


class FundForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            startYear: new Date().getFullYear(),
            retireYear: this.props.birthday.getFullYear() + parseInt(this.props.retireAge),
            // provident Fund
            providentFund: this.props.providentFund,
            provMonthlyDeposit: 0,
            providentFundAccumulation: false,
            provAccumulationFee: 0,
            provMonthlyDepositFee: 0,
            provYearlyYield: 0,
            provEmployerContributionsPercentage: 6.5,
            provEmployerContributionsValue: null,
            provEmployeeContributionsPercentage: 6.5,
            provEmployeeContributionsValue: null,
            provCompensations: 6.5,
            provCompensationsValue: null,
            //new provident
            newProvDiv: false,
            // Managers Insurance
            managersInsurance: this.props.managersInsurance,
            manMonthlyDeposit: 0,
            managersInsuranceAccumulation: false,
            manAccumulationFee: 0,
            manMonthlyDepositFee: 0,
            manYearlyYield: 0,
            manEmployerContributionsPercentage: 6.5,
            manEmployerContributionsValue: null,
            manEmployeeContributionsPercentage: 6.5,
            manEmployeeContributionsValue: null,
            manCompensations: 6.5,
            manCompensationsValue: null,
            // Managers Insurance
            pensionFund: this.props.pensionFund,
            penMonthlyDeposit: 0,
            pensionFundAccumulation: false,
            penAccumulationFee: 0,
            penMonthlyDepositFee: 0,
            penYearlyYield: 0,
            penEmployerContributionsPercentage: 6.5,
            penEmployerContributionsValue: null,
            penEmployeeContributionsPercentage: 6.5,
            penEmployeeContributionsValue: null,
            penCompensations: 6.5,
            penCompensationsValue: null,
            penComplementPensionFund: 0,
            penCompensationsValueToProvidentFund: false,
            pensionComplementFund: false,
            comAccumulationFee: 0,
            comMonthlyDepositFee: 0,
            comYearlyYield: 0,
            //pension Validition
            employeeContributionError: false,
            employerContributionError: false,
            compensationsErr: false,
            pensionMaxValue: (10273 * 2 * (20.5 / 100))
        }
    }

    updatePercentageValuesMan = (employer, employee, compensations) => {
        let monthlySalary = parseFloat(this.props.managersInsuranceValue);
        let percentageEmployer = parseFloat(employer * monthlySalary) / 100;
        let percentageEmployee = parseFloat(employee * monthlySalary) / 100;
        let percentageCompensations = parseFloat(compensations * monthlySalary) / 100;

        let monthlyDeposit = (parseFloat(percentageEmployer) + parseFloat(percentageEmployee) + parseFloat(percentageCompensations)).toFixed(2);
        //(percentageEmployer + percentageEmployee + percentageCompensations) * monthlySalary;
        this.setState({
            manEmployerContributionsValue: (percentageEmployer).toFixed(2),
            manEmployeeContributionsValue: (percentageEmployee).toFixed(2),
            manCompensationsValue: (percentageCompensations).toFixed(2),
            manMonthlyDeposit: monthlyDeposit
        })
    }

    updatePercentageValuesProv = (employer, employee, compensations) => {
        let monthlySalary = parseFloat(this.props.providentFundValue);
        let percentageEmployer = parseFloat(employer * monthlySalary) / 100;
        let percentageEmployee = parseFloat(employee * monthlySalary) / 100;
        let percentageCompensations = parseFloat(compensations * monthlySalary) / 100;

        let monthlyDeposit = (parseFloat(percentageEmployer) + parseFloat(percentageEmployee) + parseFloat(percentageCompensations)).toFixed(2);
        //(percentageEmployer + percentageEmployee + percentageCompensations) * monthlySalary;

        this.setState({
            provEmployerContributionsValue: (percentageEmployer).toFixed(2),
            provEmployeeContributionsValue: (percentageEmployee).toFixed(2),
            provCompensationsValue: (percentageCompensations).toFixed(2),
            provMonthlyDeposit: monthlyDeposit
        })
    }

    //Pension Fund
    updatePercentageValuesPen = (employer, employee, compensations) => {
        let monthlySalary = parseFloat(this.props.pensionFundValue);
        let percentageEmployer = parseFloat(employer * monthlySalary) / 100;
        let percentageEmployee = parseFloat(employee * monthlySalary) / 100;
        let percentageCompensations = parseFloat(compensations * monthlySalary) / 100;

        let monthlyDeposit, complementPensionFund = 0;
        let pensionComplementFund = false;
        let totalDeposit = (parseFloat(percentageEmployer) + parseFloat(percentageEmployee) + parseFloat(percentageCompensations)).toFixed(2);

        let provMonthlyDeposit = this.state.provMonthlyDeposit;
        let penCompensationsValue = this.state.penCompensationsValue;
        //(percentageEmployer + percentageEmployee + percentageCompensations) * monthlySalary;
        if (totalDeposit < this.state.pensionMaxValue) {
            monthlyDeposit = totalDeposit;
            complementPensionFund = 0;
            pensionComplementFund = false
        }
        else {
            monthlyDeposit = this.state.pensionMaxValue;
            complementPensionFund = parseFloat(totalDeposit) - parseFloat(this.state.pensionMaxValue);
            pensionComplementFund = true;
        }
        if (this.state.penCompensationsValueToProvidentFund) {
            provMonthlyDeposit = parseFloat(provMonthlyDeposit) - parseFloat(penCompensationsValue) + parseFloat(percentageCompensations)
            this.setState({
                provMonthlyDeposit: (provMonthlyDeposit).toFixed(2)
            })
        }
        this.setState({
            penEmployerContributionsValue: (percentageEmployer).toFixed(2),
            penEmployeeContributionsValue: (percentageEmployee).toFixed(2),
            penCompensationsValue: (percentageCompensations).toFixed(2),
            penMonthlyDeposit: monthlyDeposit,
            penComplementPensionFund: complementPensionFund,
            pensionComplementFund: pensionComplementFund,
        })
    }

    componentWillMount() {
        let monthlySalary = this.props.monthlySalary;
        // Provident Fund
        let providentFundAccumulation = false;
        let percentageEmployer = parseFloat(this.state.provEmployerContributionsPercentage * this.props.providentFundValue) / 100;
        let percentageEmployee = parseFloat(this.state.provEmployerContributionsPercentage * this.props.providentFundValue) / 100;
        let percentageCompensations = parseFloat(this.state.provCompensations * this.props.providentFundValue) / 100;
        let monthlyDeposit = (parseFloat(percentageEmployer) + parseFloat(percentageEmployer) + parseFloat(percentageEmployer)).toFixed(2);

        if (this.props.providentFund) {
            providentFundAccumulation = true
        }

        // Managers Insurance
        let managersInsuranceAccumulation = false;
        let manPercentageEmployer = parseFloat(this.state.manEmployerContributionsPercentage * this.props.managersInsuranceValue) / 100;
        let manPercentageEmployee = parseFloat(this.state.manEmployeeContributionsPercentage * this.props.managersInsuranceValue) / 100;
        let manPercentageCompensations = parseFloat(this.state.manCompensations * this.props.managersInsuranceValue) / 100;
        let manMonthlyDeposit = (parseFloat(manPercentageEmployer) + parseFloat(manPercentageEmployer) + parseFloat(manPercentageEmployer)).toFixed(2);

        if (this.props.managersInsurance) {
            managersInsuranceAccumulation = true
        }

        // Pension Fund
        let pensionFundAccumulation = false;
        let penPercentageEmployer = parseFloat(this.state.penEmployerContributionsPercentage * this.props.pensionFundValue) / 100;
        let penPercentageEmployee = parseFloat(this.state.penEmployeeContributionsPercentage * this.props.pensionFundValue) / 100;
        let penPercentageCompensations = parseFloat(this.state.penCompensations * this.props.pensionFundValue) / 100;
        // let penMonthlyDeposit = (parseFloat(penPercentageEmployer) + parseFloat(penPercentageEmployer) + parseFloat(penPercentageEmployer)).toFixed(2);
        let penMonthlyDeposit, penComplementPensionFund = 0;
        let pensionComplementFund = false;
        let totalDeposit = (parseFloat(penPercentageEmployer) + parseFloat(penPercentageEmployer) + parseFloat(penPercentageEmployer)).toFixed(2);
        if (totalDeposit < this.state.pensionMaxValue) {
            penMonthlyDeposit = totalDeposit;
        }
        else {
            penMonthlyDeposit = this.state.pensionMaxValue;
            penComplementPensionFund = parseFloat(totalDeposit) - parseFloat(this.state.pensionMaxValue);
            pensionComplementFund = true;
        }
        if (this.props.pensionFund) {
            pensionFundAccumulation = true
        }


        this.setState({
            // Provident Fund
            provEmployerContributionsValue: percentageEmployer,
            provEmployeeContributionsValue: percentageEmployee,
            provCompensationsValue: percentageCompensations,
            provMonthlyDeposit: monthlyDeposit,
            providentFundAccumulation: providentFundAccumulation,
            // Managers Insurance
            manEmployerContributionsValue: manPercentageEmployer,
            manEmployeeContributionsValue: manPercentageEmployee,
            manCompensationsValue: manPercentageCompensations,
            manMonthlyDeposit: manMonthlyDeposit,
            managersInsuranceAccumulation: managersInsuranceAccumulation,
            // Pension Fund
            penEmployerContributionsValue: penPercentageEmployer,
            penEmployeeContributionsValue: penPercentageEmployee,
            penCompensationsValue: penPercentageCompensations,
            penMonthlyDeposit: penMonthlyDeposit,
            pensionFundAccumulation: pensionFundAccumulation,
            penComplementPensionFund: penComplementPensionFund,
            pensionComplementFund: pensionComplementFund
        })
    }

    // Provident Fund

    provEmployerContributionsPChange = (event) => {
        var employerContribution = event.target.value;
        this.updatePercentageValuesProv(
            employerContribution,
            this.state.provEmployeeContributionsPercentage,
            this.state.provCompensations
        )
        this.setState({
            provEmployerContributionsPercentage: commafy(employerContribution)
        })
    }

    provEmployeeContributionsPChange = (event) => {
        var employeeContribution = event.target.value;
        this.updatePercentageValuesProv(
            this.state.provEmployerContributionsPercentage,
            employeeContribution,
            this.state.provCompensations
        )
        this.setState({
            provEmployeeContributionsPercentage: commafy(employeeContribution)
        })
    }

    provCompensationsPChange = (event) => {
        var compensationsContribution = event.target.value;
        this.updatePercentageValuesProv(
            this.state.provEmployerContributionsPercentage,
            this.state.provEmployeeContributionsPercentage,
            compensationsContribution
        )
        this.setState({
            provCompensations: commafy(compensationsContribution)
        })
    }

    handleProvidentChange = (event) => {
        let value = !this.state.providentFundAccumulation;
        if (!value) {
            this.setState({
                providentFundAccumulation: value,
                providentFund: null
            })
        }
        else {
            this.setState({ providentFundAccumulation: value })
        }
    }

    // Managers Insurance

    manEmployerContributionsPChange = (event) => {
        var employerContribution = event.target.value;
        this.updatePercentageValuesMan(
            employerContribution,
            this.state.manEmployeeContributionsPercentage,
            this.state.manCompensations
        )
        this.setState({
            manEmployerContributionsPercentage: employerContribution
        })
    }

    manEmployeeContributionsPChange = (event) => {
        var employeeContribution = event.target.value;
        this.updatePercentageValuesMan(
            this.state.manEmployerContributionsPercentage,
            employeeContribution,
            this.state.manCompensations
        )
        this.setState({
            manEmployeeContributionsPercentage: employeeContribution
        })
    }

    manCompensationsPChange = (event) => {
        var compensationsContribution = event.target.value;
        this.updatePercentageValuesMan(
            this.state.manEmployerContributionsPercentage,
            this.state.manEmployeeContributionsPercentage,
            compensationsContribution
        )
        this.setState({
            manCompensations: compensationsContribution
        })
    }

    handleManagersInsuranceChange = (event) => {
        let value = !this.state.managersInsuranceAccumulation;
        if (!value) {
            this.setState({
                managersInsuranceAccumulation: value,
                managersInsurance: null
            })
        }
        else {
            this.setState({ managersInsuranceAccumulation: value })
        }
    }

    // Pension Fund

    penEmployerContributionsPChange = (event) => {
        var employerContribution = event.target.value;
        if (employerContribution >= 6.5 && employerContribution <= 7.5) {
            this.setState({
                employerContributionError: false
            })
            this.updatePercentageValuesPen(
                employerContribution,
                this.state.penEmployeeContributionsPercentage,
                this.state.penCompensations
            )
        }
        else {
            this.setState({
                employerContributionError: true
            })

        }
        this.setState({
            penEmployerContributionsPercentage: employerContribution
        })
    }

    penEmployeeContributionsPChange = (event) => {
        var employeeContribution = event.target.value;
        if (employeeContribution >= 6 && employeeContribution <= 7) {
            this.setState({
                employeeContributionError: false
            })
            this.updatePercentageValuesPen(
                this.state.penEmployerContributionsPercentage,
                employeeContribution,
                this.state.penCompensations
            )
        }
        else {
            this.setState({
                employeeContributionError: true
            })

        }
        this.setState({
            penEmployeeContributionsPercentage: employeeContribution
        })


    }

    penCompensationsPChange = (event) => {
        var compensationsContribution = event.target.value;
        if (compensationsContribution >= 6 && compensationsContribution <= 8.33) {
            this.setState({
                compensationsErr: false
            })
            this.updatePercentageValuesPen(
                this.state.penEmployerContributionsPercentage,
                this.state.penEmployeeContributionsPercentage,
                compensationsContribution
            )
        }
        else {
            this.setState({
                compensationsErr: true
            })
        }
        this.setState({
            penCompensations: compensationsContribution
        })
    }

    handlePensionChange = (event) => {
        let value = !this.state.pensionFundAccumulation;
        if (!value) {
            this.setState({
                pensionFundAccumulation: value,
                pensionFund: null
            })
        }
        else {
            this.setState({ pensionFundAccumulation: value })
        }
    }

    penCompensationsValueToProvidentFundCheckboxChange = (event) => {
        let checked = event.target.checked
        let penMonthlyDeposit = this.state.penMonthlyDeposit
        let penComplementPensionFund = this.state.penComplementPensionFund
        let penCompensationsValue = this.state.penCompensationsValue
        let provMonthlyDeposit = this.state.provMonthlyDeposit
        let newProvDiv = this.state.newProvDiv
        let pensionComplementFund = this.state.pensionComplementFund
        if (checked) {
            let totalDeposit = (parseFloat(penMonthlyDeposit) + parseFloat(penComplementPensionFund)) - parseFloat(penCompensationsValue)
            if (totalDeposit < this.state.pensionMaxValue) {
                penMonthlyDeposit = totalDeposit;
                penComplementPensionFund = 0
            }
            else {
                penMonthlyDeposit = this.state.pensionMaxValue;
                penComplementPensionFund = parseFloat(totalDeposit) - parseFloat(this.state.pensionMaxValue);
                pensionComplementFund = true;
            }
            provMonthlyDeposit = parseFloat(provMonthlyDeposit) + parseFloat(penCompensationsValue)
            newProvDiv = true
        }
        else if (!checked) {
            let totalDeposit = parseFloat(penMonthlyDeposit) + parseFloat(penComplementPensionFund) + parseFloat(penCompensationsValue)
            if (totalDeposit < this.state.pensionMaxValue) {
                penMonthlyDeposit = totalDeposit;
                penComplementPensionFund = 0
            }
            else {
                penMonthlyDeposit = this.state.pensionMaxValue;
                penComplementPensionFund = parseFloat(totalDeposit) - parseFloat(this.state.pensionMaxValue);
                pensionComplementFund = true;
            }
            provMonthlyDeposit = parseFloat(provMonthlyDeposit) - parseFloat(penCompensationsValue)
            newProvDiv = false
        }
        this.setState({
            penCompensationsValueToProvidentFund: checked,
            penMonthlyDeposit: penMonthlyDeposit,
            penComplementPensionFund: penComplementPensionFund,
            pensionComplementFund: pensionComplementFund,
            provMonthlyDeposit: provMonthlyDeposit,
            newProvDiv: newProvDiv
        })
    }

    render() {
        const { startYear, retireYear, managersInsurance, providentFund } = this.state;
        console.log(this.props);

        return (
            <Fragment>
                <Stepper activeStep={2} />
                <Paper style={{ padding: 25 }}>
                    <span style={{ color: "#1b5e20 ", fontWeight: "bolder", fontSize: "1.2rem" }}>
                        {'Total Sum of Monthly Deposits : '}
                    </span>
                    {commafy(parseFloat((parseFloat(this.state.provMonthlyDeposit) || 0) +
                        (parseFloat(this.state.manMonthlyDeposit) || 0) + (parseFloat(this.state.penMonthlyDeposit) || 0) + (parseFloat(this.state.penComplementPensionFund) || 0)).toFixed(2))}
                </Paper>
                <br />
                {/* ------------------------------- Pension Fund ------------------------------- */}
                {this.props.selectedValues.pensionFund &&
                    <MDBJumbotron>
                        <Grid container spacing={1}>
                            <Grid item xs={6}>
                                <h3>Pension Fund</h3>
                                <span style={{ color: "#1b5e20 ", fontWeight: "bolder", fontSize: "1.2rem" }}>
                                    {'Current Balance : '}
                                </span>{commafy(this.state.pensionFund != null ? this.state.pensionFund : '0')}
                                <span style={{ color: "#1b5e20 ", fontWeight: "bolder", fontSize: "1.2rem" }}>
                                    {' Retire Year : '}
                                </span>{this.state.retireYear}
                                <Divider />
                                <MDBContainer className="ml-4 mt-2">
                                    <MDBRow>
                                        <MDBCol className="m-1" xs="4">
                                            <TextField
                                                required
                                                fullWidth
                                                // value={this.state.provAccumulationFee}
                                                label="Accumulation Fee"
                                                id="pen-accumulation-fee"
                                                InputProps={{
                                                    endAdornment: <InputAdornment position="start">%</InputAdornment>,
                                                }}
                                                placeholder={this.state.penAccumulationFee}
                                                onChange={(e) => this.setState({ penAccumulationFee: e.target.value })}
                                                defaultValue={0}
                                            />
                                        </MDBCol>
                                        <MDBCol className="m-1" xs="4">
                                            <TextField
                                                required
                                                fullWidth
                                                // value={this.state.provMonthlyDepositFee}
                                                label="Monthly Deposit Fee"
                                                id="pen-monthly-deposit"
                                                InputProps={{
                                                    endAdornment: <InputAdornment position="start">%</InputAdornment>,
                                                }}
                                                placeholder={this.state.penMonthlyDepositFee}
                                                onChange={(e) => this.setState({ penMonthlyDepositFee: e.target.value })}
                                                defaultValue={0}
                                            />
                                        </MDBCol>
                                        <MDBCol className="m-1" xs="4">
                                            <TextField
                                                required
                                                fullWidth
                                                // value={this.state.provYearlyYield}
                                                label="Yearly Yield"
                                                id="pen-yearly-yield"
                                                InputProps={{
                                                    endAdornment: <InputAdornment position="start">%</InputAdornment>,
                                                }}
                                                placeholder={this.state.penYearlyYield}
                                                onChange={(e) => this.setState({ penYearlyYield: e.target.value })}
                                                defaultValue={0}
                                            />
                                        </MDBCol>
                                    </MDBRow>
                                    <br />
                                    <span className="ml-n2 mt-5" style={{ color: "#1b5e20 ", fontWeight: "bolder", fontSize: "1.2rem" }}>
                                        {'Monthly Fund of Pension Fund :'}
                                    </span>{commafy(this.props.pensionFundValue)}
                                    <br />
                                    <span className="ml-n2 mt-5" style={{ color: "#1b5e20 ", fontWeight: "bolder", fontSize: "1.2rem" }}>
                                        {'Monthly Deposit of Provident Fund :'}
                                    </span>{this.state.penMonthlyDeposit === 0 ? 0 : commafy(parseFloat(this.state.penMonthlyDeposit).toFixed(2))}
                                    <MDBRow className="mt-4">
                                        <MDBCol className="m-1" xs="4">
                                            <TextField
                                                error={this.state.employerContributionError}
                                                required
                                                fullWidth
                                                label="Employer contributions Percentage"
                                                id="pen-employer-contributions-percentage"
                                                value={this.state.penEmployerContributionsPercentage}
                                                onChange={this.penEmployerContributionsPChange}
                                                InputProps={{
                                                    endAdornment: <InputAdornment position="start">%</InputAdornment>,
                                                }}
                                                helperText={this.state.employerContributionError ?
                                                    "Employer contributions must be between 6.5% and 7.5%"
                                                    :
                                                    commafy(this.state.penEmployerContributionsValue)
                                                }
                                            />
                                        </MDBCol>
                                        <MDBCol className="m-1" xs="4">
                                            <TextField
                                                error={this.state.employeeContributionError}
                                                required
                                                fullWidth
                                                label="Employee contributions Percentage"
                                                id="pen-employee-contributions-percentage"
                                                value={this.state.penEmployeeContributionsPercentage}
                                                onChange={this.penEmployeeContributionsPChange}
                                                InputProps={{
                                                    endAdornment: <InputAdornment position="start">%</InputAdornment>,
                                                }}
                                                helperText={this.state.employeeContributionError ?
                                                    "Employee contributions must be between 6% and 7%"
                                                    :
                                                    commafy(this.state.penEmployeeContributionsValue)}
                                            />
                                        </MDBCol>
                                        <MDBCol className="m-1" xs="4">
                                            <TextField

                                                error={this.state.compensationsErr}
                                                required
                                                fullWidth
                                                label="Compensation Percentage"
                                                id="pen-compensations"
                                                value={this.state.penCompensations}
                                                onChange={this.penCompensationsPChange}
                                                InputProps={{
                                                    endAdornment: <InputAdornment position="start">%</InputAdornment>,
                                                }}
                                                helperText={this.state.compensationsErr ?
                                                    "Compensations must be between 6% and 8.33%"
                                                    :
                                                    commafy(this.state.penCompensationsValue)}
                                            />
                                        </MDBCol>
                                    </MDBRow>
                                    <MDBRow>
                                        <MDBCol className="mt-3" xs="12">
                                            Does Accumulation Exists in Pension Fund?
                                            <RadioGroup
                                                id="pensionFund"
                                                aria-label="Does accumulation exists"
                                                name="Accumulation"
                                                value={this.state.pensionFundAccumulation}
                                                onChange={this.handlePensionChange}
                                                row
                                            >
                                                <FormControlLabel value={true} control={<Radio />} labelPlacement="end" label="Yes" />
                                                <FormControlLabel value={false} control={<Radio />} labelPlacement="end" label="No" />
                                            </RadioGroup>
                                        </MDBCol>
                                        {this.state.pensionFundAccumulation ?
                                            <MDBCol md="12">
                                                <TextField
                                                    required
                                                    fullWidth
                                                    value={this.state.pensionFund}
                                                    label="Current Balance"
                                                    id="pen-formatted-numberformat-input"
                                                    InputProps={{
                                                        inputComponent: NumberFormatCustom,
                                                    }}
                                                    onChange={(e) => this.setState({ pensionFund: e.target.value })}
                                                />
                                            </MDBCol>
                                            : null}
                                    </MDBRow>
                                    <MDBRow>
                                        <FormControlLabel
                                            control={
                                                <CustomCheckBox onChange={this.penCompensationsValueToProvidentFundCheckboxChange}
                                                    checked={this.state.penCompensationsValueToProvidentFund} />
                                            }
                                            label="Deposit Compensations Amount to a Provident Fund"
                                        />
                                    </MDBRow>
                                    {
                                        this.state.pensionComplementFund && this.state.penComplementPensionFund > 0 ?
                                            <MDBRow>
                                                <Paper style={{ padding: 30, margin: 10, border: '1px solid black' }}>
                                                    <span className="ml-n2 mt-5" style={{ color: "#F50057 ", fontWeight: "bolder", fontSize: "1.2rem" }}>
                                                        {'Complement Pension Fund :'}
                                                    </span>{commafy(parseFloat(this.state.penComplementPensionFund).toFixed(2))}
                                                    <MDBCol className="m-1" xs="4">
                                                        <TextField
                                                            required
                                                            fullWidth
                                                            // value={this.state.comAccumulationFee}
                                                            label="Accumulation Fee"
                                                            id="com-accumulation-fee"
                                                            InputProps={{
                                                                endAdornment: <InputAdornment position="start">%</InputAdornment>,
                                                            }}
                                                            placeholder={this.state.comAccumulationFee}
                                                            onChange={(e) => this.setState({ comAccumulationFee: e.target.value })}
                                                            defaultValue={0}
                                                        />
                                                    </MDBCol>
                                                    <MDBCol className="m-1" xs="4">
                                                        <TextField
                                                            required
                                                            fullWidth
                                                            // value={this.state.comMonthlyDepositFee}
                                                            label="Monthly Deposit Fee"
                                                            id="com-monthly-deposit"
                                                            InputProps={{
                                                                endAdornment: <InputAdornment position="start">%</InputAdornment>,
                                                            }}
                                                            placeholder={this.state.comMonthlyDepositFee}
                                                            onChange={(e) => this.setState({ comMonthlyDepositFee: e.target.value })}
                                                            defaultValue={0}
                                                        />
                                                    </MDBCol>
                                                    <MDBCol className="m-1" xs="4">
                                                        <TextField
                                                            required
                                                            fullWidth
                                                            // value={this.state.comYearlyYield}
                                                            label="Yearly Yield"
                                                            id="com-yearly-yield"
                                                            InputProps={{
                                                                endAdornment: <InputAdornment position="start">%</InputAdornment>,
                                                            }}
                                                            placeholder={this.state.comYearlyYield}
                                                            onChange={(e) => this.setState({ comYearlyYield: e.target.value })}
                                                            defaultValue={0}
                                                        />
                                                    </MDBCol>
                                                </Paper>
                                            </MDBRow>
                                            : null
                                    }
                                </MDBContainer>
                            </Grid>
                            <Grid style={{ marginTop: '2%' }} item xs={6}>
                                <ChartsPage
                                    startYear={startYear}
                                    retireYear={retireYear}
                                    color={'rgb(0,100,0)'}
                                    backgroundColor={'rgba(0,100,0,0.1)'}
                                    name="Pension Fund"
                                    currentBalance={this.state.pensionFund}
                                    monthlySalary={this.state.penMonthlyDeposit}
                                    accumulationFee={this.state.penAccumulationFee}
                                    monthlyDepositFee={this.state.penMonthlyDepositFee}
                                    yearlyYield={this.state.penYearlyYield}
                                />
                                {/* chart for complement pension fund */}
                                {this.state.penComplementPensionFund !== 0 &&
                                    <ChartsPage
                                        startYear={startYear}
                                        retireYear={retireYear}
                                        backgroundColor={'rgb(1.00, 0.92, 0.95,0.1)'}
                                        monthlySalary={this.state.penComplementPensionFund}
                                        accumulationFee={this.state.comAccumulationFee}
                                        monthlyDepositFee={this.state.comMonthlyDepositFee}
                                        yearlyYield={this.state.comYearlyYield}
                                        color={'#F50057'}
                                        name={"Complement Pension Fund"}
                                    />
                                }
                            </Grid>
                        </Grid>
                    </MDBJumbotron>
                }
                {/* ------------------------------- Managers Insurance ------------------------------- */}
                {this.props.selectedValues.managersInsurance &&
                    <MDBJumbotron>
                        <Grid container spacing={1}>
                            <Grid item xs={6}>
                                <h3>Managers Insurance</h3>
                                <span style={{ color: "#1b5e20 ", fontWeight: "bolder", fontSize: "1.2rem" }}>
                                    {'Current Balance : '}
                                </span>{commafy(this.state.managersInsurance != null ? this.state.managersInsurance : '0')}
                                <span style={{ color: "#1b5e20 ", fontWeight: "bolder", fontSize: "1.2rem" }}>
                                    {' Retire Year : '}
                                </span>{this.state.retireYear}
                                <Divider />
                                <MDBContainer className="ml-4 mt-2">
                                    <MDBRow>
                                        <MDBCol className="m-1" xs="4">
                                            <TextField
                                                required
                                                fullWidth
                                                // value={this.state.provAccumulationFee}
                                                label="Accumulation Fee"
                                                id="man-accumulation-fee"
                                                InputProps={{
                                                    endAdornment: <InputAdornment position="start">%</InputAdornment>,
                                                }}
                                                placeholder={this.state.manAccumulationFee}
                                                onChange={(e) => this.setState({ manAccumulationFee: e.target.value })}
                                                defaultValue={0}
                                            />
                                        </MDBCol>
                                        <MDBCol className="m-1" xs="4">
                                            <TextField
                                                required
                                                fullWidth
                                                // value={this.state.provMonthlyDepositFee}
                                                label="Monthly Deposit Fee"
                                                id="man-monthly-deposit"
                                                InputProps={{
                                                    endAdornment: <InputAdornment position="start">%</InputAdornment>,
                                                }}
                                                placeholder={this.state.manMonthlyDepositFee}
                                                onChange={(e) => this.setState({ manMonthlyDepositFee: e.target.value })}
                                                defaultValue={0}
                                            />
                                        </MDBCol>
                                        <MDBCol className="m-1" xs="4">
                                            <TextField
                                                required
                                                fullWidth
                                                // value={this.state.provYearlyYield}
                                                label="Yearly Yield"
                                                id="man-yearly-yield"
                                                InputProps={{
                                                    endAdornment: <InputAdornment position="start">%</InputAdornment>,
                                                }}
                                                placeholder={this.state.manYearlyYield}
                                                onChange={(e) => this.setState({ manYearlyYield: e.target.value })}
                                                defaultValue={0}
                                            />
                                        </MDBCol>
                                    </MDBRow>
                                    <br />
                                    <span className="ml-n2 mt-5" style={{ color: "#1b5e20 ", fontWeight: "bolder", fontSize: "1.2rem" }}>
                                        {'Monthly Fund of Provident Fund :'}
                                    </span>{commafy(this.props.managersInsuranceValue)}
                                    <br />
                                    <span className="ml-n2 mt-5" style={{ color: "#1b5e20 ", fontWeight: "bolder", fontSize: "1.2rem" }}>
                                        {'Monthly Deposit of Provident Fund :'}
                                    </span>{commafy(this.state.manMonthlyDeposit)}
                                    <MDBRow className="mt-4">
                                        <MDBCol className="m-1" xs="4">
                                            <TextField
                                                required
                                                fullWidth
                                                label="Employer contributions Percentage"
                                                id="man-employer-contributions-percentage"
                                                value={this.state.manEmployerContributionsPercentage}
                                                onChange={this.manEmployerContributionsPChange}
                                                InputProps={{
                                                    endAdornment: <InputAdornment position="start">%</InputAdornment>,
                                                }}
                                                helperText={commafy(this.state.manEmployerContributionsValue)}
                                            />
                                        </MDBCol>
                                        <MDBCol className="m-1" xs="4">
                                            <TextField
                                                required
                                                fullWidth
                                                label="Employee contributions Percentage"
                                                id="man-employee-contributions-percentage"
                                                value={this.state.manEmployeeContributionsPercentage}
                                                onChange={this.manEmployeeContributionsPChange}
                                                InputProps={{
                                                    endAdornment: <InputAdornment position="start">%</InputAdornment>,
                                                }}
                                                helperText={commafy(this.state.manEmployeeContributionsValue)}
                                            />
                                        </MDBCol>
                                        <MDBCol className="m-1" xs="4">
                                            <TextField
                                                required
                                                fullWidth
                                                label="Compensation Percentage"
                                                id="man-compensations"
                                                value={this.state.manCompensations}
                                                onChange={this.manCompensationsPChange}
                                                InputProps={{
                                                    endAdornment: <InputAdornment position="start">%</InputAdornment>,
                                                }}
                                                helperText={commafy(this.state.manCompensationsValue)}
                                            />
                                        </MDBCol>
                                    </MDBRow>
                                    <MDBRow>
                                        <MDBCol className="mt-3" xs="12">
                                            Does Accumulation Exists in Managers Insurance Fund?
                                            <RadioGroup
                                                id="manegersInsurance"
                                                aria-label="Does accumulation exists"
                                                name="Accumulation"
                                                value={this.state.managersInsuranceAccumulation}
                                                onChange={this.handleManagersInsuranceChange}
                                                row
                                            >
                                                <FormControlLabel value={true} control={<Radio />} labelPlacement="end" label="Yes" />
                                                <FormControlLabel value={false} control={<Radio />} labelPlacement="end" label="No" />
                                            </RadioGroup>
                                        </MDBCol>
                                        {this.state.managersInsuranceAccumulation ?
                                            <MDBCol md="12">
                                                <TextField
                                                    required
                                                    fullWidth
                                                    value={this.state.managersInsurance}
                                                    label="Current Balance"
                                                    id="man-formatted-numberformat-input"
                                                    InputProps={{
                                                        inputComponent: NumberFormatCustom,
                                                    }}
                                                    onChange={(e) => this.setState({ managersInsurance: e.target.value })}
                                                />
                                            </MDBCol>
                                            : null}
                                    </MDBRow>
                                </MDBContainer>
                            </Grid>
                            <Grid style={{ marginTop: '2%' }} item xs={6}>
                                <ChartsPage
                                    startYear={startYear}
                                    retireYear={retireYear}
                                    color={'#5e35b1'}
                                    backgroundColor={'rgba(63, 81, 181, 0.1)'}
                                    name="Managers Insurance"
                                    currentBalance={this.state.managersInsurance}
                                    monthlySalary={this.state.manMonthlyDeposit}
                                    accumulationFee={this.state.manAccumulationFee}
                                    monthlyDepositFee={this.state.manMonthlyDepositFee}
                                    yearlyYield={this.state.manYearlyYield}
                                />
                            </Grid>
                        </Grid>
                    </MDBJumbotron>
                }
                {/* ------------------------------- Provident Fund ------------------------------- */}
                {(this.props.selectedValues.providentFund || this.state.newProvDiv) &&
                    <MDBJumbotron>
                        <Grid container spacing={1}>
                            <Grid item xs={6}>
                                <h3>Provident Fund</h3>
                                <span style={{ color: "#1b5e20 ", fontWeight: "bolder", fontSize: "1.2rem" }}>
                                    {'Current Balance : '}
                                </span>{commafy(this.state.providentFund != null ? this.state.providentFund : '0')}
                                <span style={{ color: "#1b5e20 ", fontWeight: "bolder", fontSize: "1.2rem" }}>
                                    {' Retire Year : '}
                                </span>{this.state.retireYear}
                                <Divider />
                                <MDBContainer className="ml-4 mt-2">
                                    <MDBRow>
                                        <MDBCol className="m-1" xs="4">
                                            <TextField
                                                required
                                                fullWidth
                                                // value={this.state.provAccumulationFee}
                                                label="Accumulation Fee"
                                                id="accumulation-fee"
                                                InputProps={{
                                                    endAdornment: <InputAdornment position="start">%</InputAdornment>,
                                                }}
                                                placeholder={commafy(this.state.provAccumulationFee)}
                                                onChange={(e) => this.setState({ provAccumulationFee: e.target.value })}
                                                defaultValue={0}
                                            />
                                        </MDBCol>
                                        <MDBCol className="m-1" xs="4">
                                            <TextField
                                                required
                                                fullWidth
                                                // value={this.state.provMonthlyDepositFee}
                                                label="Monthly Deposit Fee"
                                                id="monthly-deposit"
                                                InputProps={{
                                                    endAdornment: <InputAdornment position="start">%</InputAdornment>,
                                                }}
                                                placeholder={this.state.provMonthlyDepositFee}
                                                onChange={(e) => this.setState({ provMonthlyDepositFee: commafy(e.target.value) })}
                                                defaultValue={0}
                                            />
                                        </MDBCol>
                                        <MDBCol className="m-1" xs="4">
                                            <TextField
                                                required
                                                fullWidth
                                                // value={this.state.provYearlyYield}
                                                label="Yearly Yield"
                                                id="yearly-yield"
                                                InputProps={{
                                                    endAdornment: <InputAdornment position="start">%</InputAdornment>,
                                                }}
                                                placeholder={this.state.provYearlyYield}
                                                onChange={(e) => this.setState({ provYearlyYield: commafy(e.target.value) })}
                                                defaultValue={0}
                                            />
                                        </MDBCol>
                                    </MDBRow>
                                    <br />
                                    <span className="ml-n2 mt-5" style={{ color: "#1b5e20 ", fontWeight: "bolder", fontSize: "1.2rem" }}>
                                        {'Monthly Fund of Provident Fund :'}
                                    </span>{commafy(this.props.providentFundValue)}
                                    <br />
                                    <span className="ml-n2 mt-5" style={{ color: "#1b5e20 ", fontWeight: "bolder", fontSize: "1.2rem" }}>
                                        {'Monthly Deposits of Provident Fund :'}
                                    </span>{commafy(this.state.provMonthlyDeposit)}
                                    <MDBRow className="mt-4">
                                        <MDBCol className="m-1" xs="4">
                                            <TextField
                                                required
                                                fullWidth
                                                label="Employer contributions Percentage"
                                                id="employer-contributions-percentage"
                                                value={this.state.provEmployerContributionsPercentage}
                                                onChange={this.provEmployerContributionsPChange}
                                                InputProps={{
                                                    endAdornment: <InputAdornment position="start">%</InputAdornment>,
                                                }}
                                                helperText={commafy(this.state.provEmployerContributionsValue)}
                                            />
                                        </MDBCol>
                                        <MDBCol className="m-1" xs="4">
                                            <TextField
                                                required
                                                fullWidth
                                                label="Employee contributions Percentage"
                                                id="employee-contributions-percentage"
                                                value={this.state.provEmployeeContributionsPercentage}
                                                onChange={this.provEmployeeContributionsPChange}
                                                InputProps={{
                                                    endAdornment: <InputAdornment position="start">%</InputAdornment>,
                                                }}
                                                helperText={commafy(this.state.provEmployeeContributionsValue)}
                                            />
                                        </MDBCol>
                                        <MDBCol className="m-1" xs="4">
                                            <TextField
                                                required
                                                fullWidth
                                                label="Compensation Percentage"
                                                id="compensations"
                                                value={this.state.provCompensations}
                                                onChange={this.provCompensationsPChange}
                                                InputProps={{
                                                    endAdornment: <InputAdornment position="start">%</InputAdornment>,
                                                }}
                                                helperText={commafy(this.state.provCompensationsValue)}
                                            />
                                        </MDBCol>
                                    </MDBRow>
                                    <MDBRow>
                                        <MDBCol className="mt-3" xs="12">
                                            Does Accumulation Exists in Provident Fund?
                                            <RadioGroup
                                                id="providentFund"
                                                aria-label="Does accumulation exists"
                                                name="Accumulation"
                                                value={this.state.providentFundAccumulation}
                                                onChange={this.handleProvidentChange}
                                                row
                                            >
                                                <FormControlLabel value={true} control={<Radio />} labelPlacement="end" label="Yes" />
                                                <FormControlLabel value={false} control={<Radio />} labelPlacement="end" label="No" />
                                            </RadioGroup>
                                        </MDBCol>
                                        {this.state.providentFundAccumulation ?
                                            <MDBCol md="12">
                                                <TextField
                                                    required
                                                    fullWidth
                                                    value={this.state.providentFund}
                                                    label="Current Balance"
                                                    id="formatted-numberformat-input"
                                                    InputProps={{
                                                        inputComponent: NumberFormatCustom,
                                                    }}
                                                    onChange={(e) => this.setState({ providentFund: e.target.value })}
                                                />
                                            </MDBCol>
                                            : null}
                                    </MDBRow>
                                </MDBContainer>
                            </Grid>
                            <Grid style={{ marginTop: '2%' }} item xs={6}>
                                <ChartsPage
                                    startYear={startYear}
                                    retireYear={retireYear}
                                    color={'#ef6c00'}
                                    backgroundColor={'rgba(255, 152, 0, 0.1)'}
                                    name="Provident Fund"
                                    currentBalance={this.state.providentFund}
                                    monthlySalary={this.state.provMonthlyDeposit}
                                    accumulationFee={this.state.provAccumulationFee}
                                    monthlyDepositFee={this.state.provMonthlyDepositFee}
                                    yearlyYield={this.state.provYearlyYield}
                                />
                            </Grid>
                        </Grid>
                    </MDBJumbotron>
                }
            </Fragment>
        )
    }
}

export default connect(mapStateToProps)(FundForm);