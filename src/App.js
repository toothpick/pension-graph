import React, { Fragment } from 'react';
import './App.css';

import { BrowserRouter, Route, Switch } from "react-router-dom";
import FormPage from './components/form';
import splitFunds from './components/splitFunds';
import FundForm from './components/fundForm';

function App(props) {
  return (
    <Fragment>
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={FormPage} />
          <Route path="/splitfunds" component={splitFunds} />
          <Route path="/linechart" component={FundForm} />
        </Switch>
      </BrowserRouter>
    </Fragment>
  );
}

export default App;
