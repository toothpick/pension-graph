import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

// MDBReact Imports 
import "@fortawesome/fontawesome-free/css/all.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";

import App from './App';
import * as serviceWorker from './serviceWorker';

// Redux Imports
import { Provider } from 'react-redux';
import { createStore } from 'redux';

// Redux Store
const initialState = {
    birthday: new Date('01-30-1995'),
    retireAge: 60,
    monthlySalary: null,
    selectedValues: {
        pensionFund: false,
        managersInsurance: false,
        providentFund: false,
    },
};

function reducer(state = initialState, action) {
    switch (action.type) {
        case 'SAVE_INFORMATION':
            return {
                ...state,
                selectedValues: {
                    ...state.selectedValues,
                    pensionFund: action.selectedValues.pensionFund,
                    managersInsurance: action.selectedValues.managersInsurance,
                    providentFund: action.selectedValues.providentFund
                },
                birthday: action.birthday,
                retireAge: action.retireAge,
                monthlySalary: action.monthlySalary,
                pensionFundValue: action.pensionFundValue,
                managersInsuranceValue: action.managersInsuranceValue,
                providentFundValue: action.providentFundValue,
            }
        case 'MAKE_CHARTS':
            return {
                ...state,
                selectedValues: {
                    ...state.selectedValues
                },
                birthday: state.birthday,
                retireAge: state.retireAge,
                monthlySalary: state.monthlySalary,
                pensionFund: action.pensionFund,
                managersInsurance: action.managersInsurance,
                providentFund: action.providentFund,
            }
        default:
            return state;
    }
}

const store = createStore(reducer);

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
